# Annotate - Application Android de Bloc-Notes

<img src="https://gitlab.com/JanikStLouis/annotate-apk/-/raw/main/Annotate-logos.jpeg" width="200" height="200" alt="Logo de l'application Annotate">

Annotate est une application Android de bloc-notes simple et puissante qui vous permet de prendre des notes, d'organiser vos idées, et de les annoter avec facilité.

## Fonctionnalités

- **Prise de notes rapide :** Prenez des notes en quelques secondes, que ce soit pour des réunions, des idées, des listes de tâches, ou des notes personnelles.

- **Menu simple d'utilisation :** Naviguez dans vos notes dans un menu simple d'utilisation.

- **Ajoutez votre Localisation à vos notes :** (**Nécéssite** l'autorisation de Localisation dans les paramètres Android)

- **Partage de notes :** Partagez vos notes à n'importe qui rapidement depuis Annotate.

- **Ouvrez une nouvelle note depuis votre soulignement de texte :** Soulignez du texte partour sur android, appuyez sur partager puis appuyez sur l'application Annotate. Une note sera alors créé automatiquement


## Installation

Pour installer Annotate, suivez ces étapes simples :

1. Téléchargez le fichier APK depuis [notre Gitlab contenant l'APK](https://gitlab.com/JanikStLouis/annotate-apk/-/blob/main/Annotate.apk?ref_type=heads).

2. Installez l'application sur votre appareil Android.

3. Lancez l'application et commencez à prendre des notes !

## Configuration requise

- minimum sdk --> 24 android 7.0 (Nougat) ou version ultérieure.

## Licence

Ce projet est sous licence MIT. Pour plus de détails, consultez le fichier [LICENSE](LICENSE).

## Contact

Pour toute question ou suggestion, n'hésitez pas à nous contacter à [janikstlouis@gmail.com].

---
Nous espérons que vous trouverez Annotate utile pour vos besoins de prise de notes. Merci de l'avoir choisi !
